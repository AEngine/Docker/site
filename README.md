#### Create network
```
docker network create nginx
```

#### Development:
Start containter:
```
docker-compose -f docker-compose.yml -f docker-compose.development.yml up -d
```

Stop containter:
```
docker-compose -f docker-compose.yml -f docker-compose.development.yml down
```


#### Production:
Start containter:
```
docker-compose -f docker-compose.yml -f docker-compose.production.yml up -d
```

Stop containter:
```
docker-compose -f docker-compose.yml -f docker-compose.production.yml down
```